#include <iostream>

using namespace std;

string email[] = {
  "h.aimarmaulana@gmail.com",
  "haffidz.aimar.maulana.tik22@mhsw.pnj.ac.id",
  "dewomaulana77@outlook.com"
};

string password[] = {
  "INIAKUNUTAMA",
  "iniAkunPNJ",
  "iniAkunMicrosoft"
};

int dataLen = sizeof(email) / sizeof (string);

void label() {
  cout << "==============================\n";
  cout << "======  login   system  ======\n";
  cout << "== by haffidz aimar maulana ==\n";
  cout << "==============================\n\n";
}

bool isCorrect(string eml, string pass) {
  for (int i = 0; i < dataLen; i++) {
    if (eml == email[i] && pass == password[i]) {
      return true;

      break;
    }
  }

  return false;
}

int main() {
  string inputEmail, inputPass;
  int count = 1;

  label();

  cout << "input your email    : "; cin >> inputEmail;
  cout << "input your password : "; cin >> inputPass;

  while (!isCorrect(inputEmail, inputPass)) {
    cout << "your email or password does not recorded!\n";
    cout << "input your email    : "; cin >> inputEmail;
    cout << "input your password : "; cin >> inputPass;

    count++;

    if (count == 3) {
      break;
    }
  }

  if (isCorrect(inputEmail, inputPass)) {
    system("cls");
    
    cout << "passed\n\n";
    cout << "hello " << inputEmail << "!\n"; 
  }

  if (count == 3) {
    system("cls");

    cout << "you have tried " << count << " times\n";
    cout << "please try again later!\n";
  }

  return 0;
}