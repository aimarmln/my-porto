#include <iostream>
using namespace std;

void label() {
  cout << "==============================\n";
  cout << "======  sign up system  ======\n";
  cout << "== by haffidz aimar maulana ==\n";
  cout << "==============================\n\n";
}

bool checkEmail(string email) {
  string emailTemplate[] = {
    "@gmail.com",
    "@yahoo.com",
    "@outlook.com",
    "@mhsw.pnj.ac.id"
  };

  int tempLen = sizeof(emailTemplate) / sizeof(string);
  int count;

  for (int i = 0; i < tempLen; i++) {
    count = 0;

    for (int j = email.length() - 1, k = emailTemplate[i].length() - 1; j > (email.length() - 1) - emailTemplate[i].length(), k >= 0; j--, k--) {
      if (email[j] == emailTemplate[i][k]) {
        count++;
      }

      if (count == emailTemplate[i].length()) {
        return true;

        break;
      }
    }
  }

  return false;
}

bool checkPass(string pass) {
  const string symbols = "!@#$%^&*";
  const string numbers = "0123456789";
  const string caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  int symbolsLen = symbols.length();
  int numbersLen = numbers.length();
  int capsLen = caps.length();
  int count = 0;

  const int passwordLength = 8;

  for (int i = 0; i < pass.length(); i++) {
    for (int j = 0; j < symbolsLen; j++) {
      if (pass[i] == symbols[j]) {
        count++;
      }
    }

    for (int k = 0; k < numbersLen; k++) {
      if (pass[i] == numbers[k]) {
        count++;
      }
    }

    for (int l = 0; l < capsLen; l++) {
      if (pass[i] == caps[l]) {
        count++;
      }
    }
  }

  if (pass.length() >= passwordLength && count > 0) {
    return true;
  } else {
    return false;
  }
}

int main() {
  string inputEmail, inputPass;

  label();

  cout << "input email    : "; cin >> inputEmail;
  cout << "input password : "; cin >> inputPass;

  while (!checkEmail(inputEmail) || !checkPass(inputPass)) {
    cout << endl;

    if (!checkEmail(inputEmail) && !checkPass(inputPass)) {
      cout << "your email and password are not valid\n";
      cout << "input email    : "; cin >> inputEmail;
      cout << "input password : "; cin >> inputPass;
    } else if (!checkEmail(inputEmail)) {
      cout << "your email is not valid\n";
      cout << "input email    : "; cin >> inputEmail;
    } else if (!checkPass(inputPass)) {
      cout << "your password is not valid\n";
      cout << "the password must contains 8 characters and at least 1 symbol, 1 number, and 1 upper case\n";
      cout << "input password : "; cin >> inputPass;
    }
  }

  system("cls");
  cout << "passed\n\n";
  cout << "your email     : " << inputEmail << endl;
  cout << "your password  : " << inputPass << endl;

  return 0;
}