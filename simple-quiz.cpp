#include <iostream>
using namespace std;

string questions[] = {
  "1. How do we print \"hello world\" in C++?\n",
  "2. How do we pronounce \"cout\" and \"cin\"?\n",
  "3. What is the area of the trapezium above?\n"
};

string answers[] = {
  "c. cout << \"hello world\";\n",
  "b. see out and see in\n",
  "a. 80\n"
};

const string indent = "   ";

void label() {
  cout << "==============================\n";
  cout << "======== simple  quiz ========\n";
  cout << "== by haffidz aimar maulana ==\n";
  cout << "==============================\n";
}

void quizToken() {
  const string token = "a$Up4F1r3";
  string inputToken;

  cout << "insert token : "; cin >> inputToken;

  while (inputToken != token) {
    cout << "your token does not match!\n";
    cout << "insert token : "; cin >> inputToken;
  }
}

void quesOne() {
  cout << questions[0];
  cout << indent << "a. print(\"hello world\")\n";
  cout << indent << "b. console.log(\"hello world\");\n";
  cout << indent << answers[0];
  cout << indent << "d. <h1>hello world</h1>\n";
  cout << "\nyour answer: \n";
}

void quesTwo() {
  cout << questions[1];
  cout << indent << "a. kawt and kin\n";
  cout << indent << answers[1];
  cout << indent << "c. cowt and cin\n";
  cout << indent << "d. ce out and ce in\n";
  cout << "\nyour answer: \n";
}

void trapz() {
  for (int i = 1; i <= 5; i++) {
    for (int j = 1; j <= 10; j++) {
      cout << '*';
    }
    for (int k = 1; k <= i; k++) {
      cout << "**";
    }

    cout << endl;
  }
}

void quesThree() {
  trapz();

  cout << questions[2];
  cout << "" << answers[2];
  cout << "b. 110\n";
  cout << "c. 130\n";
  cout << "d. 120\n";
  cout << "\nyour answer: \n";
}

bool checkAns(string ans) {
  char x = toupper(ans[0]);

  if ((ans.length() == 1) && (x == 'A' || x == 'B' || x == 'C' || x == 'D')) {
    return true;
  } else {
    return false;
  }
}

bool yOrN(string inp) {
  char x = toupper(inp[0]);

  if ((inp.length() == 1) && (x == 'Y' || x == 'N')) {
    return true;
  } else {
    return false;
  }
}

float score(string one, string two, string three) {
  string myAns[] = {one, two, three};
  float ansLen = sizeof(myAns) / sizeof(myAns[0]);
  float count = 0;
  
  for (int i = 0; i < ansLen; i++) {
    if (myAns[i][0] == answers[i][0]) {
      count++;
    }
  }

  return (count / ansLen) * 100;
}

string index(float scr) {
  if (scr > 90) {
    return "A (excellent, keep it up!)";
  } else if ( scr > 60) {
    return "B (good work!)";
  } else {
    return "C (get better next time!)";
  }
}

void summary(string one, string two, string three) {
  string myAns[] = {one, two, three};
  int quizLen = sizeof(questions) / sizeof(string);
  int count = 0;

  for (int i = 0; i < quizLen; i++) {
    string result = (myAns[i][0] == answers[i][0]) ? "(correct)" : "(false)";

    if (myAns[i][0] == answers[i][0]) {
      count++;
    }

    if (questions[i][0] == '3') {
      trapz();
    }

    cout << questions[i];
    cout << indent << "correct answer: " << answers[i];
    cout << indent << "your answer: " << myAns[i] << " " << result << endl << endl;
  }

  if (count == 0) {
    cout << "you did not answer a single question correctly!\n\n";
  } else {
    cout << "you answered " << count << " of " << quizLen << " questions correctly!\n\n";
  }
}

int main() {
  string ansOne, ansTwo, ansThree, again;

  label();
  quizToken();
  
  system("cls");

  do {
    quesOne();
    cin >> ansOne;
    while (!checkAns(ansOne)) {
      cout << "your answer: (please input a, b, c, or d!)\n";
      cin >> ansOne;
    }

    system("cls");

    quesTwo();
    cin >> ansTwo;
    while (!checkAns(ansTwo)) {
      cout << "your answer: (please input a, b, c, or d!)\n";
      cin >> ansTwo;
    }

    system("cls");

    quesThree();
    cin >> ansThree;
    while (!checkAns(ansThree)) {
      cout << "your answer: (please input a, b, c, or d!)\n";
      cin >> ansThree;
    }

    system("cls");

    cout << "your score: " << score(ansOne, ansTwo, ansThree) << endl;
    cout << "your index: " << index(score(ansOne, ansTwo, ansThree)) << endl;
    cout << "\nsummary: \n\n";
    
    summary(ansOne, ansTwo, ansThree);

    do {
      cout << "wanna play again? (y/n): "; cin >> again;
    } while (!yOrN(again));

    if (toupper(again[0]) == 'Y') {
      system("cls");
    } else {
      system("cls");
      
      cout << "have a nice day!\n";

      break;
    }

  } while (toupper(again[0]) == 'Y');

  return 0;
}